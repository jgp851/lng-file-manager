#pragma once
#include <vector>

class LNGfile;

#ifdef LNGFILEMNG_EXPORTS
#    define LNGFILEMNG_DLLCALL __declspec(dllexport)
#else
#    define LNGFILEMNG_DLLCALL __declspec(dllimport)
#endif

class LNGfileManager
{
private:
	std::vector<LNGfile*> LNGfiles;
	unsigned short int selectedFile;
public:
	LNGFILEMNG_DLLCALL LNGfileManager(const char *path);
	LNGFILEMNG_DLLCALL ~LNGfileManager();
	
	LNGFILEMNG_DLLCALL const char* operator[](int i);
	LNGFILEMNG_DLLCALL const char* getCurrentLanguageLocal();
	LNGFILEMNG_DLLCALL short int getLanguageCode();
	LNGFILEMNG_DLLCALL void setLanguageFile(unsigned short int selectFile);
	LNGFILEMNG_DLLCALL void setLanguageFileByCode(short int languageCode);
	LNGFILEMNG_DLLCALL void setNextLanguage();
	LNGFILEMNG_DLLCALL void setPreviousLanguage();
};