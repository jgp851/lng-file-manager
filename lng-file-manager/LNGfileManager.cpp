#include "LNGfileManager.h"
#include "LNGfile.h"
#include <dirent.h>
#include <string>
#include "..\..\JGP.iostream\JGP.iostream\jgp.iostream.h"

LNGfileManager::LNGfileManager(const char *path)
{
	selectedFile = 0;
	std::vector<const char*> *files = jgp_iostr_fileListing(path, true);
	for(unsigned int i = 0; i < files->size(); i++)
		LNGfiles.push_back(new LNGfile((*files)[i]));
}
LNGfileManager::~LNGfileManager()
{
	for(std::vector<LNGfile*>::iterator it = LNGfiles.begin(); it != LNGfiles.end(); it++)
		delete *it;
}

const char* LNGfileManager::operator[](int i)
{
	if(i >= 0 && i < LNGfiles[selectedFile]->getStringCount())
		return (*LNGfiles[selectedFile])[i];
	else
		return "unknown text";
}

const char* LNGfileManager::getCurrentLanguageLocal()
{
	return LNGfiles[selectedFile]->getLanguageLocalName();
}

short int LNGfileManager::getLanguageCode()
{
	return LNGfiles[selectedFile]->getLanguageCode();
}

void LNGfileManager::setLanguageFile(unsigned short int selectFile)
{
	selectedFile = selectFile;
}

void LNGfileManager::setLanguageFileByCode(short int languageCode)
{
	bool set = false;
	for(unsigned int i = 0; i < LNGfiles.size(); i++)
		if(LNGfiles[i]->getLanguageCode() == languageCode)
		{
			set = true;
			selectedFile = i;
			break;
		}
	if(!set)
		selectedFile = 0;
}

void LNGfileManager::setNextLanguage()
{
	selectedFile++;
	if(selectedFile == LNGfiles.size())
		selectedFile = 0;
}

void LNGfileManager::setPreviousLanguage()
{
	if(selectedFile == 0)
		selectedFile = LNGfiles.size() - 1;
	else
		selectedFile--;
}