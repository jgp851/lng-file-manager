#pragma once
#include <string>
#include <vector>

class LNGitem;

class LNGfile
{
private:
	short languageCode;
	int stringCount;
	int stringAddress;
	std::string languageName;
	std::string languageLocalName;
	std::vector<LNGitem*> itemList;
public:
	LNGfile(const char *path);
	~LNGfile();
	short getLanguageCode();
	int getStringCount();
	const char* getLanguageName();
	const char* getLanguageLocalName();
	const char* operator[](int i);
};