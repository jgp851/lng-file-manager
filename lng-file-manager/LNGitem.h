#pragma once
#include <string>
#include <fstream>

class LNGitem
{
private:
	int textAddress;
	std::string text;
public:
	LNGitem();
	~LNGitem();
	
	const char* getString();

	void readHeader(std::ifstream &file);
	void readString(std::ifstream &file);
};