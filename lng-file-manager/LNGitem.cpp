#include "LNGitem.h"
#include "..\..\jgp-std-math\jgp-std-math\JGPstdMath.h"

LNGitem::LNGitem()
{
}
LNGitem::~LNGitem()
{
}

const char* LNGitem::getString()
{
	return text.c_str();
}

void LNGitem::readHeader(std::ifstream &file)
{
	file.read((char*)&textAddress, 4);
}

void LNGitem::readString(std::ifstream &file)
{
	short len;
	file.read((char*)&len, 2);
	len = jgp_std_math_convertEndianShort(len);
	char *buffer = new char[len + 1];
	file.read(buffer, len);
	buffer[len] = '\0';
	text = buffer;
	delete[] buffer;
}