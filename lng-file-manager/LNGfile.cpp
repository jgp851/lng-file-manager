#include "LNGfile.h"
#include "LNGitem.h"
#include <fstream>
#include "..\..\jgp-std-math\jgp-std-math\JGPstdMath.h"

LNGfile::LNGfile(const char *path)
{
	std::ifstream file(path, std::ios::binary);
	if(file)
	{
		file.read((char*)&languageCode, 2);
		file.read((char*)&stringCount, 4);
		file.read((char*)&stringAddress, 4);

		short len;
		char *buffer;
		// string languageName
		file.read((char*)&len, 2);
		len = jgp_std_math_convertEndianShort(len);
		buffer = new char[len + 1];
		file.read(buffer, len);
		buffer[len] = '\0';
		languageName = buffer;
		delete[] buffer;
		
		// string languageLocalName
		file.read((char*)&len, 2);
		len = jgp_std_math_convertEndianShort(len);
		buffer = new char[len + 1];
		file.read(buffer, len);
		buffer[len] = '\0';
		languageLocalName = buffer;
		delete[] buffer;
		
		// utworzenie obiekt�w LNGstring i zapisanie do wektora stringList
		for(short i = 0; i < stringCount; i++)
			itemList.push_back(new LNGitem());
		// odczytywanie nag��wk�w string�w
		for(short i = 0; i < stringCount; i++)
			itemList[i]->readHeader(file);
		// odczytywanie string�w
		for(int i = 0; i < stringCount; i++)
			itemList[i]->readString(file);
		file.close();
	}
}
LNGfile::~LNGfile()
{
	for(std::vector<LNGitem*>::iterator it = itemList.begin(); it != itemList.end(); it++)
		delete *it;
}

const char* LNGfile::operator[](int i)
{
	if(i >= 0 && i < (int)itemList.size())
		return itemList[i]->getString();
	else
		return "unknown text";
}

short LNGfile::getLanguageCode()
{
	return languageCode;
}

int LNGfile::getStringCount()
{
	return stringCount;
}

const char* LNGfile::getLanguageName()
{
	return languageName.c_str();
}

const char* LNGfile::getLanguageLocalName()
{
	return languageLocalName.c_str();
}